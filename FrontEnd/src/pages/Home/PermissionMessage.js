import {Button, Grid, Paper, Typography} from "@material-ui/core";
import {Close as CloseIcon} from "@material-ui/icons";
import React from "react";

export const PermissionMessage = (props) => {
    return (
        <Grid container style={{position: "fixed", height: "100%"}} justify="center" alignItems="center">
            <Grid item>
                <Paper style={{padding: "20px"}} elevation={3}>
                    <Grid container item direction="column" alignItems="center" justify="space-evenly"
                          style={{width: "auto"}}>
                        <Grid item style={{alignSelf: "flex-end"}}>
                            <Button onClick={props.onClose} color="primary"
                                    style={{fontSize: "1.5rem", fontWeight: "100"}}>
                                <CloseIcon style={{width: "3rem", height: "3rem"}}></CloseIcon>
                            </Button>
                        </Grid>
                        <Grid item>
                            <Typography>
                                For this site to work you need to give device orientation permissions.
                            </Typography><br/>
                            <Typography>
                                Go to Safari Settings > Privacy And Security and enable Motion & Orientation Access.
                            </Typography>
                            <Typography>
                                Or press...
                            </Typography><br/><br/>
                            <Button variant ="outlined" color="primary"fullWidth onClick={props.onRequest}>Request Permission</Button>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    )
}