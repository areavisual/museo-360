#FrontEnd Build Installation Manual
To install the FrontEnd Build provided, follow this steps:
####1 - Decompress and Upload FrontEnd
Decompress and Upload the content of the ```FrontEnd.zip``` file provided in this bundle, to the server.<br>
For this manual we will consider ```/var/www/museum/``` as the base path on the server. Then if
you upload the FrontEnd folder (which is inside ```FrontEnd.zip```) at ```/var/www/museum/``` your should end up with ```/var/www/museum/FrontEnd/...``` on the server.<br><br>
####2 - Configure Nginx
Configure Nginx to point to the BackEnd folder by creating a new file called ```api-domain.conf``` (replace api-domain with the actual api domain, Eg.```api.360museum.com.conf```)
inside the ```/etc/nginx/sites-available/``` folder.<br><br>
Open it and paste:
```
server {
    server_name <domain>;
    root  <frontend-path>;
        index index.html index.htm;
    
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    charset utf-8;

    location / {
       try_files $uri /index.html;
    }
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/default-error.log error;

    error_page 404 /index.php;

    location ~ /\.(?!well-known).* {
        deny all;
    }
}

```
<b>NOTE:</b>

- Replace ```<domain>``` with the domains (with and without ```www``` separated by spaces) where the FrontEnd will be hosted. (Eg. ```www.museum.com museum.com```)
- Replace ```<backend-path>``` with the actual path to the build folder inside the FrontEnd folder.(Eg. ```/var/www/museum/FrontEnd/build/```)
####3 - Install SSL Certificate for the Backend
Install a certificate for the api-domain and configure nginx to use this SSL Certificate.
####4 - Check and Restart Nginx
Check the configuration of Nginx by running:

```nginx -t```

If there are no errors you can proceed by running

```systemctl restart nginx```

Once you've done all, the website should be online.