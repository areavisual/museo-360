#FrontEnd Installation Manual
To install the FrontEnd on the server you will need:
 - Node.js v.10.22.0 or higher
 - Nginx v.1.15.8 or higher
##Installation
In order to install the 360 Museum FrontEnd follow this steps:
####1 - Upload FrontEnd
Upload the ```FrontEnd``` folder provided in this bundle inside the ````core``` folder, to the server.<br>
For this manual we will consider ```/var/www/museum/``` as the base path on the server. Then if
you upload the FrontEnd folder at ```/var/www/museum/``` your should end up with ```/var/www/museum/FrontEnd/...``` on the server.<br><br>
Once inside the folder on the server, run:

```npm install```

This will install all the needed dependencies.

####2 - Configure FrontEnd
Copy and paste the ```.env.example``` found in ```/var/www/museum/FrontEnd/``` folder, then rename it to ```.env```.<br>
Once you open it, you will see something like this:
```
REACT_APP_PUBLIC=
REACT_APP_SERVER=
REACT_APP_API=
REACT_APP_ENV =production


REACT_APP_CONTACT =
REACT_APP_INSTAGRAM =
REACT_APP_HOMEPAGE =

NODE_PATH=src/
REACT_APP_DEBUG=false
```
Change only the following:

- REACT_APP_PUBLIC: The public domain where the FrontEnd will be hosted (Eg. ```https://museum.com```).
- REACT_APP_SERVER: The public domain where the BackEnd will be hosted (Eg. ```https://api.museum.com```).
- REACT_APP_API: The public domain where the BackEnd will be hosted plus the ```/api``` suffix (Eg. ```https://api.museum.com/api```).
- REACT_APP_CONTACT: The email to use when a user clicks on the Contact Button on the FrontEnd. (Eg. ```info@bluebox-soft.com```).
- REACT_APP_INSTAGRAM: The url to be visited when the user clicks on the Instagram Button the FrontEnd.
- REACT_APP_HOMEPAGE: The url to be visited when the user clicks on the HomePage Button the FrontEnd.

<b>NOTE:</b><br>
If you need to change any of these settings, you will have to repeat all the steps from here to the end.

####3 - Compiling FrontEnd
Run the command:
```npm run build```
This will create a build folder with the fresh copy of the FrontEnd
####4 - Configure Nginx
Configure Nginx to point to the BackEnd folder by creating a new file called ```api-domain.conf``` (replace api-domain with the actual api domain, Eg.```api.360museum.com.conf```)
inside the ```/etc/nginx/sites-available/``` folder.<br><br>
Open it and paste:
```
server {
    server_name <domain>;
    root  <frontend-path>;
        index index.html index.htm;
    
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    charset utf-8;

    location / {
       try_files $uri /index.html;
    }
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/default-error.log error;

    error_page 404 /index.php;

    location ~ /\.(?!well-known).* {
        deny all;
    }
}

```
<b>NOTE:</b>

- Replace ```<domain>``` with the domains (with and without ```www``` separated by spaces) where the FrontEnd will be hosted. (Eg. ```www.museum.com museum.com```)
- Replace ```<backend-path>``` with the actual path to the build folder inside the FrontEnd folder.(Eg. ```/var/www/museum/FrontEnd/build/```)
####5 - Install SSL Certificate for the Backend
Install a certificate for the api-domain and configure nginx to use this SSL Certificate.
####6 - Check and Restart Nginx
Check the configuration of Nginx by running:

```nginx -t```

If there are no errors you can proceed by running

```systemctl restart nginx```

Once you've done all, the website should be online.

 

