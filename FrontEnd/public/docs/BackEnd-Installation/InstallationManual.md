#BackEnd Installation Manual
To install the 360 Museum BackEnd, you'll have to have a server with the following requirements:

- Composer PHP Package Manager
- PHP v.7.3 or higher
- PHP Extension
    - bz2
    - curl
    - fileinfo
    - gd2
    - mbstring
    - openssl
    - pdo_pgsql
    - pgsql
- PostgresSQL 12 or higher
- Nginx v.1.15.8 or higher
- FrontEnd & BackEnd SSL Certificates
- FrontEnd & BackEnd Domains
- A server with at least 2 GB RAM and Linux (preferred).

##Installation
In order to install the 360 Museum Backend follow this steps:

####1 - Create a Database
Create a new database in Postgres12+, with any name. 

####2 - Upload BackEnd
Upload the ```BackEnd``` folder provided inside this bundle inside the ````core``` folder, to the server.<br>
For this manual we will consider ```/var/www/museum/``` as the base path on the server. Then if
you upload the BackEnd folder at ```/var/www/museum/``` your should end up with ```/var/www/museum/BackEnd/...``` on the server.<br><br>
Once inside the folder on the server, run:

```composer install```

This will install all the needed dependencies.

####3 - Configure BackEnd
Copy and paste the ```.env.example``` found in ```/var/www/museum/BackEnd/``` folder, then rename it to ```.env```.<br>
Once you open it, you will see something like this:
```
APP_NAME=
APP_ENV=production
APP_KEY=
APP_DEBUG=false
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

CACHE_DRIVER=file
QUEUE_CONNECTION=sync

ADMIN_PASSWORD=
```
Change only the following:

- APP_NAME: Application Name.
- DB_HOST: IP or URL to access the Database. (By Default should be 127.0.0.1)
- DB_PORT: Port to access database. (By Default should be 5432)
- DB_USERNAME: User to access the database.
- DB_PASSWORD: Password to access the database.
- ADMIN_PASSWORD: Password to access the 360 Museum Administration Dashboard.

####4 - Migrate Database
Inside the ```/var/www/museum/BackEnd/``` folder run the command:<br>

```php artisan migrate```

This will create every table in the specified database.
Then run:

```php artisan key:generate```

to generate an application key.

and finally, run:

```php artisan storage:link```

####5 - Load Database Backup
Using any method (command line or GUI client), load and execute ,in Postgres, the ```backup.sql``` file inside the ```resources``` folder.
This will create the initial data needed.<br><br>
Once you've done this, copy the ```resources/images``` folder and paste it inside ```/BackEnd/storage/app/public/```. You should now have
the following: ```/BackEnd/storage/app/public/images``` in the server.

####6 - Configure Nginx
Configure Nginx to point to the BackEnd folder by creating a new file called ```api-domain.conf``` (replace api-domain with the actual api domain, Eg.```api.360museum.com.conf```)
inside the ```/etc/nginx/sites-available/``` folder.<br><br>
Open it and paste:
```
server {
    server_name <api-domain>;
    root <backend-path>;

    client_max_body_size 100M;
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    charset utf-8;    

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/default-error.log error;

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }

    location /storage/ {
        add_header 'Access-Control-Allow-Origin' '*';
    }
}
```
<b>NOTE:</b>

- Replace ```<api-domain>``` with the actual domain where the api will be hosted. (Eg. ```api.360museum.com```)
- Replace ```<backend-path>``` with the actual path to the public folder inside the BackEnd folder.(Eg. ```/var/www/museum/BackEnd/public/```)
- Notice that the line ```fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;``` is specifying that the version of php installed and that will be used is php 7.3, you should modify this line if that's not the case.
####7 - Install SSL Certificate for the Backend
Install a certificate for the api-domain and configure nginx to use this SSL Certificate.
####8 - Check and Restart Nginx
Check the configuration of Nginx by running:

```nginx -t```

If there are no errors you can proceed by running

```systemctl restart nginx```

 ####9 - Configure PHP
Open the php.ini file (which can be found in ```/etc/php/7.x/fpm``` folder, depending on the version) and uncomment (remove the ```;```):
```
extension=bz2
extension=curl
extension=fileinfo
extension=gd2
extension=mbstring
extension=openssl
extension=pdo_pgsql
extension=pgsql
```
And be sure this php extensions are installed.

Also set this values:
```
memory_limit = 1024M
post_max_size = 300M
upload_max_filesize = 200M
``` 

####10 - Test Api
Open a browser or use a client like Postman to request the available rooms by sending a request to:

```https://<api-domain>/api/rooms```

 Eg:
  
  ```https://api.museum.com/api/rooms``` 

You should get a json response if every thing is correctly set up. 
