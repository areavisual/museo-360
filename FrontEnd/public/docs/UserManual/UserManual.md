#User Manual - 360º Museum
##Introduction
This document will instruct the user on how to make use of all the features
provided by the 360º Museum Web application.
It will be devided in two main subjects, the Viewer and the Administration Panel,
each one with it's own subsections.

At the end of this document, the user will know how to use the 360º Viewer
and also how to create and opperate the administration tools to create new 
interactive experiences.

##Main Concepts
There a four main elements that are needed to understand how the 360º Museum Web
Application works.
1. <b>Picture:</b> Consider a picture as the actual artwork that will be shown on each
floor inside the museum.
2. <b>Panorama:</b> 360º image that represent a room, or a point inside a room, where a visitor can "stand" and look around. 
3. <b>Floor:</b> Composed of a set of panoramas.
4. <b>Slot:</b> Consider a slot as the physical space on a wall, where a picture is "hanged". It could
be considered as a frame.

The logic behind this concepts is that a <b>Floor</b> can have multiple <b>Panoramas</b>, each one
has one or more available <b>Slots</b>, and each Slot must have one <b>Picture</b>.

This way a complete Floor is defined.

Visitors can move around between Panoramas, and click or tap inside each Slot
to reveal the Picture information.

##The Viewer
A visitor can access the viewer by visiting the application main URL.
The first time a visitor enters the website, a loading screen will be shown
with a progress bar indicating that the assets are being fetched. Once all assets
are loaded, the viewer will show the first Panorama.
###The Help
Every time the visitor enters the page, a help dialog will be displayed. This
help dialog can be closed by the visitor, or it will dissapear by itself after 2.5 seconds.
It displays the basic controls to use the 360º Viewer.
![Viewer with Help Dialog](images/help.png)
###Viewer Layout
![Viewer Layout](images/layout.png)
<br><br>
- Minimap: The minimap is a 2D representation of the Floor the visitor is currently in.
It allows the visitor to know where he is and what direction he is looking. The red
dot indicates the visitor's position and the white cone shows where he is facing. The cone will
automatically rotate when the visitor does.<br><br>
- Main Website: this button allows the user to return to the main website.
- Floor Menu: this button displays a menu with all available floors a visitor can go to.
- Room Indicators: This indicators allow a visitor to go to another room (panorama) when clicked or tapped.
- Picture: When a visitor clicks or taps on a picture, a picture dialog will be displayed:<br><br>
    ![Viewer with picture opened](images/picture.png)
###Controls
####Rotation
#####Desktop
Clicking and dragging will move the camera in any direction.
#####Mobile
- If the user is using an IOS device and sensor permission were granted, then moving the phone 
will cause the camera to rotate along.
- If the user is using an IOS device and sensor permission were not granted, then tapping on
the screen and dragging will rotate de camera around.
- If the user is using an Android device then moving the phone 
will cause the camera to rotate along.
####Indicators
There are two kinds of indicators:
1. Room Indicator: A room indicator allows the visitor to move to another room (panorama) when clicked or tapped.<br><br>
    ![Room Indicator](images/roomIndicators.png)
2. Picture Indicator: A Picture indicator is a faded rectangle that will appear when the user
hovers over a picture. When clicked, it will display the picture information on a Picture dialog.<br><br>
    ![Picture Indicator](images/pictureIndicator.png)

###Floor Selector
At the top right corner, a button will be shown when there are more than 1 floor available to visit. This button will be visible
inspite of the visitor's location.
Also, a room indicator will appear in every panorama that has an elevator on it.
<br><br>
![Elevator](images/elevator.png)
<br><br>
Both indicators will display the same floor menu when clicked.
When a visitor clicks or taps on another floor, the viewer will be resetted, and the new floor will be loaded.
To go back, the visitor can either use the top right corner button or the indicator over the elevator.
<br><br>
![Menu](images/menu.png)
<br><br>

##Administration Panel
To enter into the administration panel ```/admin``` should be added to de URL.
Upon entering this address, the user will be asked to enter the administration password.
This password is provided in the ```Backend/.env``` file.<br><br>
![Login](images/adminPass.png)
<br><br>
Once the user has logged in, the following will be shown.
<br><br>
![Admin Panel](images/admin.png)

###Creating/Editing a Picture

###Creating/Editing a Floor








