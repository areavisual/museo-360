<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points', function ($table) {
            $table->enum('imageType', ['cube', 'image'])->default('image');
            $table->float('rs_pos_x')->nullable(); //rs room_selector.
            $table->float('rs_pos_y')->nullable();
            $table->float('rs_pos_z')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points', function ($table) {
            $table->dropColumn('imageType');
            $table->dropColumn('rs_pos_x','rs_pos_y','rs_pos_z');
        });
    }
}
