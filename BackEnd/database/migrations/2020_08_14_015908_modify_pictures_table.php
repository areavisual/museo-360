<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pictures', function( Blueprint $table){
           $table->string('linkTitle')->nullable();
           $table->dropColumn('date');
           $table->renameColumn('description', 'info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pictures', function( Blueprint $table){
            $table->date('date')->nullable();
            $table->dropColumn('linkTitle');
            $table->renameColumn( 'info','description');
        });
    }
}
