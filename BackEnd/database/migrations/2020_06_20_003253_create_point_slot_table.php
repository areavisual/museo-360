<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointSlotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_slot', function (Blueprint $table) {
            $table->bigInteger('slot_id');
            $table->bigInteger('point_id');
            $table->foreign('slot_id')->references('id')->on('slots')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['slot_id', 'point_id']);

            $table->boolean('is_start')->default(false);
            $table->float('pos_x')->default(0);
            $table->float('pos_y')->default(0);
            $table->float('pos_z')->default(0);
            $table->float('rot_x')->default(0);
            $table->float('rot_y')->default(0);
            $table->float('rot_z')->default(0);
            $table->float('scale_x')->default(0);
            $table->float('scale_y')->default(0);
            $table->float('scale_z')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_slot');
    }
}
