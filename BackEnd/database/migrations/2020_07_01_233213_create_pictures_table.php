<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('slot_id')->nullable()->uniqid();
            $table->bigInteger('room_id')->nullable();
            $table-> foreign('slot_id')->references('id')->on('slots')->onDelete('set null')->onUpdate('cascade');
            $table-> foreign('room_id')->references('id')->on('rooms')->onDelete('set null')->onUpdate('cascade');
            $table->string('image');
            $table->string('title');
            $table->string('size');
            $table->string('description')->nullable();
            $table->string('link')->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');

    }
}
