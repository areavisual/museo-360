<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_point', function (Blueprint $table) {
            $table->bigInteger('current_point_id');
            $table->foreign('current_point_id')->references('id')->on('points')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('other_point_id');
            $table->foreign('other_point_id')->references('id')->on('points')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['current_point_id','other_point_id']);
            $table->enum('type', ["room", "point"])->default(0);
            $table->float('pos_x')->default(0);
            $table->float('pos_y')->default(0);
            $table->float('pos_z')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_points');
    }
}
