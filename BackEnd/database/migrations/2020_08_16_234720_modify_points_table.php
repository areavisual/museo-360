<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points', function($table){
            $table->float('map_pos_x')->nullable();
            $table->float('map_pos_y')->nullable();
            $table->float('map_pos_z')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points', function($table){
            $table->dropColumn('map_pos_x', 'map_pos_y', 'map_pos_z');
        });
    }
}
