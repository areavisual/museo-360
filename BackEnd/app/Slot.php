<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    //
    public function points(){
        return $this->belongsToMany(
            Point::class)
            ->using(PointSlot::class)
            ->withPivot('is_start','pos_x','pos_y','pos_z','rot_x','rot_y','rot_z','scale_x','scale_y','scale_z');
    }
    public function picture(){
        return $this->hasOne(Picture::class);
    }
}
