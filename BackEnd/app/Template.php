<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'templates';

    public function rooms(){
        return $this->hasMany(Room::class);
    }

    public function points(){
        return $this->hasMany(Point::class)->orderBy('id', 'asc');
    }

    // public function slots(){
    //     return $this->hasManyThrough('App\Slot','App\Point_Slot','template_id', 'point_id', 'id', 'id')->select("slot_id")->distinct();
    // }
}
