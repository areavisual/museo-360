<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PointSlot extends Pivot
{
    protected $casts = [
        'pos_x' => 'float',
        'pos_y' => 'float',
        'pos_z' => 'float',
        'rot_x' => 'float',
        'rot_y' => 'float',
        'rot_z' => 'float',
        'scale_x' => 'float',
        'scale_y' => 'float',
        'scale_z' => 'float'
    ];

    protected $table = "point_slot";

    public function slot(){
        return $this->hasOne(Slot::class,"id", "slot_id");
    }
}
