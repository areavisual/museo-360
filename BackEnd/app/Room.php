<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function template(){
        return $this->belongsTo(Template::class);
    }
    public function pictures(){
        return $this->hasMany(Picture::class);
    }

    public function points() //particular represetation of a template point for a particular room... here we can save and get the image of that point for a specific room.this allows to have multiple rooms with same template but different panorama with different pictures on them.
    {
        return $this->belongsToMany(Point::class)->withPivot('image');
    }
}
