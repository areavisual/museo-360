<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    //
    protected $fillable = ['image'];
    public function slot(){
        return $this->belongsTo(Slot::class);
    }

    public function room(){
        return $this->belongsTo(Room::class);
    }

}
