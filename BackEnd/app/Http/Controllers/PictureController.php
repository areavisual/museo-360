<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PictureController extends Controller
{
    public function index(Request $request)
    {$rowsPerPage = (int)$request->get("rowsPerPage");
        $page = (int)$request->get("page");
        $filters = json_decode($request->get("filters"));
        $order = json_decode($request->get("order"));
        if(!is_null($filters)){
            $query = Picture::where("id", "<>", null);
            if(!is_null($filters->title))
                $query = $query->where("title","like", "%".$filters->title."%");
            if (!is_null($filters->date))
                $query = $query->where("date", "=", $filters->date);
            if (!is_null($filters->size))
                $query = $query->where("size", "=", $filters->size);
            if (!is_null($filters->available)){
                if ($filters->available)
                    $query = $query->doesntHave("room");
                else
                    $query = $query->has("room");
            }
        }
        else{
            $query = Picture::where("id", "!=", null);
        }
        $total = $query->count();

        if(!is_null($order)){
            $query = $query->orderBy($order->column, $order->direction);
        }
        else{
            $query = $query->orderBy('id', 'asc');
        }
        if($page!==null && $rowsPerPage!==null && $rowsPerPage !== 0)
            $query = $query->offset($page * $rowsPerPage)->limit($rowsPerPage);

        $pictures = $query->get();
        return response()->json(compact(["pictures", "total"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = (array) json_decode($request->get("data"));
        $data = array_merge($data, ["image"=> $request->file('image')]);
        $rules=[
            "title" => "required|string",
            "info" => "nullable|string",
            "link" => "nullable|string|url",
            "linkTitle" => "nullable|required_with:link|string",
            "size" => "required|string",
            "image" => "required|file|max:200000",
        ];
        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        try {
            DB::beginTransaction();
            $picture = new Picture();
            $picture->title = $data["title"];
            $picture->info = $data["info"];
            $picture->link = $data["link"];
            $picture->linkTitle = $data["linkTitle"];
            $picture->size = $data["size"];

            if ($request->hasFile('image')) {
                if($request->file('image')->isValid()){
                    $image = $request->file('image');
                    $path = str_replace( 'public', 'storage',$image->storeAs("public/images", uniqid().'.'.$image->getClientOriginalExtension()));
                    $picture->image = $path;
                }
            }
            $picture->save();
            DB::commit();
            return response()->json(compact('picture'));
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $picture = Picture::findOrFail($id);
        return response()->json(compact('picture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = (array) json_decode($request->get("data"));
        $data = array_merge($data, ["image" => $request->file('image')]);
        $rules = [
            "title" => "required|string",
            "info" => "nullable|string",
            "link" => "nullable|string|url",
            "linkTitle" => "nullable|required_with:link|string",
            "size" => "required|string",
            "image" => "nullable|file|max:200000",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        try {
            DB::beginTransaction();
            $picture = Picture::findOrFail($id);
            $picture->title = $data["title"];
            $picture->info = $data["info"];
            $picture->link = $data["link"];
            $picture->linkTitle = $data["linkTitle"];
            $picture->size = $data["size"];


            if ($request->hasFile('image')) {
                if($request->file('image')->isValid()){
                    $oldImageName = explode('/',$picture->image);
                    $oldImageName = storage_path('app/public/images'). "/" .$oldImageName[sizeOf($oldImageName)-1];
                    File::delete($oldImageName);
                    $image = $request->file('image');
                    $path = str_replace( 'public', 'storage',$image->storeAs("public/images", uniqid().'.'.$image->getClientOriginalExtension()));
                    $picture->image = $path;
                }
            }
            $picture->save();
            DB::commit();
            return response()->json(compact('picture'));
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $picture = Picture::findOrFail($id);
            $oldImageName = explode('/', $picture->image);
            //$oldImageName = base_path('public\images') . "\\" . $oldImageName[sizeOf($oldImageName) - 1];
            $oldImageName = storage_path('app/public/images'). "/" . $oldImageName[sizeOf($oldImageName) - 1];

            File::delete($oldImageName);
            Picture::destroy($id);
            DB::commit();
            return response()->json(compact('picture'));

        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
