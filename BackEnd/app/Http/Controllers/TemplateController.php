<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\Point;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::with([
            "points.slots.picture"
            ])
            ->where('is_selectable', '=', true)
            ->get();

        return response()->json(compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $template = Template::with('points.slots')->where('id', $id)->firstOrFail();

        return response()->json(compact('template'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
    }

    public function download($point_id){
        $point = Point::findOrFail($point_id);
        $path = storage_path(str_replace("storage/", "app/public/", $point->image));
        return Response::download($path);
    }
}
