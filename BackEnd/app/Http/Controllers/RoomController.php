<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Room;
use App\Picture;
use App\Point;
use App\Slot;
use Intervention\Image\Facades\Image;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowsPerPage = (int)$request->get("rowsPerPage");
        $page = (int)$request->get("page");
        //$filters = json_decode($request->get("filters"));
        $order = json_decode($request->get("order"));
        //select [] discards any selection, while withCount adds a new select for the count so if you need to only select the count, use this combination
        //select all rooms and adds a field called slots, with the amount of pictures that can be added to the template.
        $query = Room::with(["template"])
            ->addSelect(
                ["slots" => Point::select([])
                    ->withCount("slots")
                    ->whereColumn("template_id", "rooms.template_id")
                    ->orderBy('slots_count', 'desc')->limit(1)]
            );
        $total = $query->count();

        if (!is_null($order)) {
            $query = $query->orderBy($order->column, $order->direction);
        } else {
            $query = $query->orderBy('id', 'asc');
        }
        if ($page !== null && $rowsPerPage !== null && $rowsPerPage !== 0)
            $query = $query->offset($page * $rowsPerPage)->limit($rowsPerPage);
        $rooms = $query->get();
        return response()->json(compact(["rooms", "total"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = json_decode($request->get("data"));
        $data->images = $request->file("images");
        $rules = [
            "name" => "required|string",
            "template" => "required|exists:templates,id",
            "pictures" => "required",
            "points" => "required",
            "images" => "required"
        ];
        $validator = Validator::make((array)$data, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            DB::beginTransaction();

            $room = new Room();
            $room->name = $data->name;
            $room->template_id = (int)$data->template;
            $room->save();

            foreach ($data->pictures as $aux) {
                $picture = Picture::findOrFail($aux->id);
                $picture->room()->associate($room);
                $picture->slot_id = $aux->slot_id;
                $picture->save();
            }

            foreach ($data->points as $point) {
                $_image = array_values(array_filter($data->images, function ($img) use ($point) {
                    $name = $img->getClientOriginalName();
                    return $name == $point->image;
                }))[0];
                $path = $this->uploadPointImages($point, $_image);
                $room->points()->attach($point->point_id, ["image" => $path]);
            }
            $room->save();

            DB::commit();
            return response()->json(compact("room"));
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = json_decode($request->get("data"));
        $data->images = $request->file("images");
        $rules = [
            "name" => "required|string",
            "template" => "required|exists:templates,id",
            "pictures" => "required"
        ];
        $validator = Validator::make((array)$data, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            DB::beginTransaction();

            $room = Room::with("points")->findOrfail($id);
            $room->name = $data->name;
            $room->template_id = (int)$data->template;
            $room->save();
            $room->refresh();

            $current_pictures = Picture::where("room_id", $room->id)->get()->pluck("id")->toArray();

            $new_pictures = array_map(function ($obj) {
                return $obj->id;
            }, $data->pictures);

            $_delete = array_values(array_diff($current_pictures, $new_pictures));
            $_add = array_values(array_diff($new_pictures, $current_pictures));
            $_modify = array_values(array_intersect($current_pictures, $new_pictures));

            // Erase all asignations from $_delete
            Picture::whereIn('id', $_delete)->update(['room_id' => null, 'slot_id' => null]);

            // Add assignations for every picture in $_add
            $_add = Picture::findMany($_add);
            foreach ($_add as $picture) {
                $picture->room()->associate($room);
                $slot_id = array_values(array_filter($data->pictures, function ($p) use ($picture) {
                    return $p->id == $picture->id;
                }))[0]->slot_id;
                $picture->slot_id = $slot_id;
                $picture->save();
            }

            //Modify assignations for every picture in $_modify only if it's slot was changed
            $_modify = Picture::findMany($_modify);
            foreach ($_modify as $picture) {
                $slot_id = array_values(array_filter($data->pictures, function ($p) use ($picture) {
                    return $p->id === $picture->id;
                }))[0]->slot_id;
                if ($slot_id !== $picture->slot_id) {
                    $picture->slot_id = $slot_id;
                    $picture->save();
                }
            }

            $current_points = $room->points()->pluck("point_id")->toArray();
            $template_points = $room->template->points()->pluck("id")->toArray(); //new template because of room->refresh()

            //delete
            $_delete = array_values(array_diff($current_points, $template_points));
            $_modify = array_values(array_diff($current_points, $_delete));

            //Delete Old Images
            foreach ($room->points as $point){
                if(in_array($point->point_id, $_delete)){
                    $this->deletePointImages($point);
                }
            }

            $room->points()->detach($_delete);
            $room->save();

            foreach ($data->points as $index => $point) {
                if (in_array($point->point_id, $_modify)) {
                    //Delete Old Image
                    $pointToDelete = $room->points->first(function ($p) use ($point) {
                        return $p->id === $point->point_id;
                    });
                    $this->deletePointImages($pointToDelete);

                    //Upload new image
                    $image = array_values(array_filter($data->images, function ($i) use ($point) {
                        return $i->getClientOriginalName() === $point->image;
                    }))[0];
                    $path = $this->uploadPointImages($point, $image);
                    $room->points()->updateExistingPivot($point->point_id, ["image" => $path]);
                }
            }

            $room->save();

            DB::commit();
            return response()->json(compact("room"));
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $room = Room::with(['template', 'points.destination_points', 'points.slots.picture'])->where('id', $id)->firstOrFail();
        return response()->json(compact("room"));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $room = Room::findOrFail($id);
            foreach ($room->points as $point) {
                $this->deletePointImages($point);
            }
            Room::destroy($id);
            DB::commit();
            return response()->json(compact("room"));
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    private function uploadPointImages($point, $_image){
        $path = null;
        if ($point->imageType === 'cube') {
            $uuid = uniqid();
            $image = Image::make($_image);
            $cubeSize = $image->getHeight() / 3;
            $image->backup();
            $posX = $image->crop($cubeSize, $cubeSize, 2 * $cubeSize, $cubeSize);
            $posX->save("storage/images/posX_" . $uuid . '.' . $_image->getClientOriginalExtension());
            $image->reset();

            $negX = $image->crop($cubeSize, $cubeSize, 0, $cubeSize);
            $negX->save("storage/images/negX_" . $uuid . '.' . $_image->getClientOriginalExtension());

            $image->reset();
            $posY = $image->crop($cubeSize, $cubeSize, $cubeSize, 0);
            $posY->save("storage/images/posY_" . $uuid . '.' . $_image->getClientOriginalExtension());

            $image->reset();
            $negY = $image->crop($cubeSize, $cubeSize, $cubeSize, 2 * $cubeSize);
            $negY->save("storage/images/negY_" . $uuid . '.' . $_image->getClientOriginalExtension());

            $image->reset();
            $posZ = $image->crop($cubeSize, $cubeSize, $cubeSize, $cubeSize);
            $posZ->save("storage/images/posZ_" . $uuid . '.' . $_image->getClientOriginalExtension());

            $image->reset();
            $negZ = $image->crop($cubeSize, $cubeSize, 3 * $cubeSize, $cubeSize);
            $negZ->save("storage/images/negZ_" . $uuid . '.' . $_image->getClientOriginalExtension());
            $image->destroy();
            $path = 'storage/images/' . $uuid . '.' . $_image->getClientOriginalExtension();
        } else {
            $path = str_replace('public', 'storage', $_image->storeAs("public/images", uniqid() . '.' . $_image->getClientOriginalExtension()));
        }
        return $path;
    }

    private function deletePointImages($point){
        $oldImageName = $point->pivot->image;
        $suffixes = explode('/', $oldImageName);
        $suffix = $suffixes[count($suffixes)-1];
        $posX = storage_path('app/public/images'). "/" ."posX_". $suffix;
        $posY = storage_path('app/public/images'). "/" ."posY_". $suffix;
        $posZ = storage_path('app/public/images'). "/" ."posZ_". $suffix;
        $negX = storage_path('app/public/images'). "/" ."negX_". $suffix;
        $negY = storage_path('app/public/images'). "/" ."negY_". $suffix;
        $negZ = storage_path('app/public/images'). "/" ."negZ_". $suffix;
        File::delete([$posX, $posY, $posZ, $negX, $negY, $negZ]);
    }
}
