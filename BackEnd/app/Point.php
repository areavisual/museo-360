<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $casts = [
        'rot_x' => 'float',
        'rot_y' => 'float',
        'rot_z' => 'float'
    ];

    public function template(){
        return $this->belongsTo(Template::class);
    }
    public function slots(){
        return $this->belongsToMany(Slot::class)->using(PointSlot::class)->withPivot('is_start','pos_x','pos_y','pos_z','rot_x','rot_y','rot_z','scale_x','scale_y','scale_z')->orderBy('id', 'asc');
    }
    public function destination_points(){
        return $this->belongsToMany(
            Point::class,
            'point_point',
            'current_point_id',
            'other_point_id')
            ->using(PointPoint::class)
            ->withPivot(['type', 'pos_x', 'pos_y', 'pos_z']);
    }

    public function points(){
        return $this->belongsToMany(
            Point::class,
            'point_point',
            'other_point_id',
            'current_point_id')
            ->using(PointPoint::class)
            ->withPivot(['type', 'pos_x', 'pos_y', 'pos_z']);
    }

    public function rooms(){
        return $this->belongsToMany(Room::class)->withPivot('image');
    }

}
