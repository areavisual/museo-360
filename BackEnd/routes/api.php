<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    return ['version' => '0.1'];
});
Route::get('rooms', 'RoomController@index');
Route::post('rooms', 'RoomController@create');
Route::get('rooms/{id}', 'RoomController@read');
Route::put('rooms/{id}', 'RoomController@update');
Route::delete('rooms/{id}', 'RoomController@delete');

Route::post('pictures', 'PictureController@create');
Route::get('pictures', 'PictureController@index');
Route::get('pictures/{id}', 'PictureController@read');
Route::put('pictures/{id}', 'PictureController@update');
Route::delete('pictures/{id}', 'PictureController@delete');

Route::get('templates', 'TemplateController@index');
Route::get('templates/{id}', 'TemplateController@read');
Route::get('templates/download/{slot_id}', 'TemplateController@download');
Route::post('login', 'AuthController@login');
